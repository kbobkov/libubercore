#ifndef __UC_INCLUDE__
#define __UC_INCLUDE__





//  ------------------------------------------------------------------------
//  BUILD OPTIONS
//  ------------------------------------------------------------------------
#ifndef UPRO_DEFINES_ENABLED
//  VARIANTS:
//  USYS_WINDWOS
//  USYS_WINDOWS_8
//  USYS_LINUX
//  USYS_APPLE_IOS
//  USYS_APPLE_MAC
#   define USYS_APPLE_MAC
#endif

#define USYS_DEBUG





//  ------------------------------------------------------------------------
//  BASE DEFINES
//  ------------------------------------------------------------------------
#ifdef USYS_WINDOWS
#   define USYS_PLATFORM_WINDOWS
#endif

#ifdef USYS_LINUX
#   define USYS_PLATFORM_UNIX
#endif

#ifdef USYS_APPLE_MAC
#   define USYS_PLATFORM_UNIX
#   define USYS_PLATFORM_APPLE
#endif

#ifdef USYS_APPLE_IOS
#   define USYS_PLATFORM_UNIX
#   define USYS_PLATFORM_APPLE
#endif

#define public_signals      public
#define protected_signals   protected
#define private_signals     private
#define private_slots       private





//  ------------------------------------------------------------------------
//  SYSTEM INDEPENDENT INCLUDES
//  ------------------------------------------------------------------------
#include <iostream>
#include <iomanip>
#include <fstream>
#include <ostream>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <map>
#include <list>
#include <queue>
#include <iterator>
using namespace std;


typedef unsigned char  uchar;
typedef unsigned short ushort;
typedef unsigned int   uint;







//  ------------------------------------------------------------------------
//  SYSTEM DEPENDENT INCLUDES
//  ------------------------------------------------------------------------
#ifdef USYS_WINDOWS
#   define _WIN32_WINNT 0x0500
#   include <winsock2.h>
#   include <windows.h>

    typedef __int8          i8;
    typedef __int16         i16;
    typedef __int32         i32;
    typedef __int64         i64;
    typedef unsigned i8     u8;
    typedef unsigned i16    u16;
    typedef unsigned i32    u32;
    typedef unsigned i64    u64;
    typedef float           f32;
    typedef double          f64;

    typedef HANDLE          sys_thread;
#endif

#ifdef USYS_PLATFORM_UNIX
#   define INFINITE 0xFFFFFFFF
    typedef timespec timespec_t;

    typedef int8_t          i8;
    typedef int16_t         i16;
    typedef int32_t         i32;
    typedef int64_t         i64;
    typedef u_int8_t        u8;
    typedef u_int16_t       u16;
    typedef u_int32_t       u32;
    typedef u_int64_t       u64;
    typedef float           f32;
    typedef double          f64;

    typedef pthread_t       sys_thread;

#   include <time.h>
#   include <pthread.h>
#   include <sys/time.h>
#   include <signal.h>
#   include <errno.h>
#endif

#ifdef USYS_PLATFORM_APPLE
    typedef int pthread_spinlock_t;

#   include <time.h>
#   include <mach/mach_time.h>
#   include <unistd.h>
#   include <pthread.h>
#   include <sys/time.h>
#   include <errno.h>
#endif

#endif
