#ifndef __UC_EVENTS__
#define __UC_EVENTS__

#include "Base.h"
#include "Meta.h"


#define F_DECL  typename... P
#define F_PARAM P...



namespace ubercore
{
    //  ------------------------------------------------------------------------
    //  UBERCORE EVENT TEMPLATE CLASS
    //  ------------------------------------------------------------------------
#   define U_EVENT_BINDING  int
#   define U_EVENT_CALL     0
    
    class UEventQueue;    
    template<F_DECL> class UEvent
    {
    public:
        UEvent   ();
        UEvent   ( const UEvent<F_PARAM> &inFunction );
        ~UEvent  ();

        //  bind    - directly bind method to event
        //  connect - connects method & event using ubercore threading model
        
        template<class T> void  connect     ( T *inObject, void( T::*inFunction )(F_PARAM), UEventQueue *inQueue = NULL );
        template<class T> void  bind        ( T *inObject, void( T::*inFunction )(F_PARAM) );
        void                    disconnect  ();
        bool                    isEmpty     ();
        bool                    isEqual     ( void *inObject, void *inPtr );
        
        void                    operator()  ( F_PARAM inParams );
        void                    operator=   ( UEvent<F_PARAM> &inFunction );
    private:              
        class F {};
        typedef void ( F::*Function )( F_PARAM );
        enum _QState { QueueDisabled, QueueStatic, QueueDynamic };
        
        F               *object;
        Function        function;
        UEventQueue     *queue;
        _QState         queueState;
        
        template<typename...T> void runEvent( T... inParams );
    };
    
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE EVENT
    //  ------------------------------------------------------------------------    
    template<class T,F_DECL>
    UEvent<F_PARAM>* u_create_event( T *inObject, void ( T::*inFunction )(F_PARAM) )
    {
        UEvent<F_PARAM> *d = new UEvent<F_PARAM>();
        d->template connect<T>( inObject, inFunction );
        return d;
    }
    
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE EVENT CLASS
    //  ------------------------------------------------------------------------
    class UQueueEvent
    {
        friend class UEventQueue;
    protected:
        virtual ~UQueueEvent() {}
        virtual void processEvent() = 0;
    };
    
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE CALLABLE TEMPLATE CLASS
    //  ------------------------------------------------------------------------
    template<F_DECL> struct UCallable
    {
        template<class T> struct _Lamda: public UCallable<F_PARAM>
        {
        public:
            _Lamda  ( T *inCallable );
            ~_Lamda ();
            UCallable<F_PARAM>*     copy();
            void                    call( P... inParams );
        private:
            T *ptr;
        };
        
        virtual ~UCallable() {}
        virtual UCallable<F_PARAM>* copy() = 0;
        virtual void                call( P... inParams ) = 0;
    };
    
    template<F_DECL,class C> UCallable<F_PARAM>* ULamda( C &&inCallable )
    {
        typedef typename UCallable<F_PARAM>::template _Lamda<C> _LamdaC;
        
        C *_lamdaCode = new C(inCallable);
        return (UCallable<F_PARAM>*)(new _LamdaC( _lamdaCode ));
    }
    
    
    //  PUBLIC
    //  ------------------------------------------------------------------------
    
    template<F_DECL>
    template<class T>
    UCallable<F_PARAM>::_Lamda<T>::_Lamda( T *inPtr ) :
        ptr( inPtr )
    {
    }
    
    template<F_DECL>
    template<class T>
    UCallable<F_PARAM>::_Lamda<T>::~_Lamda()
    {
        if( ptr != NULL ) {
            delete ptr;
            ptr = NULL;
        }
    }
    
    template<F_DECL>
    template<class T>
    UCallable<F_PARAM>* UCallable<F_PARAM>::_Lamda<T>::copy()
    {
        T *destptr = new T( *ptr );
        return (UCallable<F_PARAM>*)new _Lamda<T>( destptr );
    }
    
    template<F_DECL>
    template<class T>
    void UCallable<F_PARAM>::_Lamda<T>::call( P... inParams )
    {
        ptr->operator()( inParams... );
    }

    
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE SIGNAL EVENT CLASS
    //  ------------------------------------------------------------------------
    template<typename T> class UQueueCallback: public UQueueEvent
    {
    public:
        UQueueCallback  ();
        ~UQueueCallback ();
        
        template<class C> void set( C &&inCallable, T *inParam = NULL );
        template<class C> void set( C *inCallable,  T *inParam = NULL );
    private:
        typedef T _CParam;
        typedef UCallable<_CParam*> _CBaseCallable;
        
        _CBaseCallable  *callable;
        _CParam         *param;
        
        void processEvent();
    };
    
    //  PUBLIC
    //  ------------------------------------------------------------------------
    
    template<typename T>
    UQueueCallback<T>::UQueueCallback() :
        callable( NULL ),
        param   ( NULL )
    {
    }
    
    template<typename T>
    UQueueCallback<T>::~UQueueCallback()
    {
        if( callable != NULL ) {
            delete callable;
        }
    }
    
    template<typename T>
    template<class C>
    void UQueueCallback<T>::set( C &&inCallable, T *inParam )
    {
        typedef typename _CBaseCallable::template _Lamda<C> _CCallable;
        
        C *t = new C( inCallable );
        callable    = new _CCallable( t );
        param       = inParam;
    }
    
    template<typename T>
    template<class C>
    void UQueueCallback<T>::set( C *inCallable, T *inParam )
    {
        typedef typename _CBaseCallable::template _Lamda<C> _CCallable;
        
        callable = new _CCallable( inCallable );
        param    = inParam;
    }
    
    //  PRIVATE
    //  ------------------------------------------------------------------------
    
    template<typename T>
    void UQueueCallback<T>::processEvent()
    {
        callable->call( param );
    }
    
    
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE EVENT QUEUE CLASS
    //  ------------------------------------------------------------------------
    const i32 uc_EventQueueProcessAll = -1;
    
    class UEventQueue
    {
    public:
        UEventQueue () {}
        ~UEventQueue() {}
        
        u32  countOfEvents  ();
        void add            ( UQueueEvent *inEvent );
        void clear          ();
        i32  wait           ( i32 inTimeout = uc_ConditionWaitInfinite );
        void fire           ();
        void process        ( i32 inProcessCount = uc_EventQueueProcessAll );
    private:
        list<UQueueEvent*>  queue;
        ULock               lock;
        UCondition          queueEvent;
    };
 
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE EVENT TEMPLATE CLASS REALIZATION
    //  ------------------------------------------------------------------------
    
    namespace uprivate {
        template<bool _B> struct UGetQueue{};
        template<> struct UGetQueue<true> {
            static inline UEventQueue* queue( UObject *inObject, int *inState ) {
                //  Dynamic Queue
                (*inState) = 2;
                return inObject->getDispatchQueue();
            }
        };

        template<> struct UGetQueue<false> {
            static inline UEventQueue* queue( void *inObject, int *inState ) {
                //  Disabled Queue
                (*inState) = 0;
                return NULL;
            }
        };
    }
    
    //  PUBLIC
    //  ------------------------------------------------------------------------
    
    template<F_DECL>
    UEvent<F_PARAM>::UEvent() :
        object      ( NULL ),
        queue       ( NULL ),
        queueState  ( QueueDisabled )
    {
    }
    
    template<F_DECL>
    UEvent<F_PARAM>::UEvent( const UEvent<F_PARAM> &inFunction ) :
        object      ( NULL )
    {
        object      = inFunction.object;
        function    = inFunction.function;
        queue       = inFunction.queue;
        queueState  = inFunction.queueState;
    }
    
    template<F_DECL>
    UEvent<F_PARAM>::~UEvent()
    {
        disconnect();
    }
    
    template<F_DECL>
    template<class T>
    void UEvent<F_PARAM>::connect( T *inObject, void ( T::*inFunction )(F_PARAM), UEventQueue *inQueue )
    {
        object      = (F*)( inObject );
        function    = reinterpret_cast <void (F::*)(F_PARAM)>( inFunction );
        if( inQueue != NULL ) {
            queue       = inQueue;
            queueState  = QueueStatic;
        }
        else {
            //  [Compile time expression]
            //  specialized template for true & false that
            //  calls inObject->queue or returns NULL
            queue = uprivate::UGetQueue< UIsBaseClass<UObject,T>::Yes >::queue(inObject,(int*)&queueState);
        }
    }
    
    template<F_DECL>
    template<class T>
    void UEvent<F_PARAM>::bind( T *inObject, void ( T::*inFunction )(F_PARAM) )
    {
        object      = (F*)( inObject );
        function    = reinterpret_cast <void (F::*)(F_PARAM)>( inFunction );
        queue       = NULL;
        queueState  = QueueDisabled;
    }
    
    template<F_DECL>
    void UEvent<F_PARAM>::disconnect()
    {
        object      = NULL;
        function    = NULL;
        queue       = NULL;
        queueState  = QueueDisabled;
    }

    template<F_DECL>
    bool UEvent<F_PARAM>::isEmpty()
    {
        return (object == NULL);
    }
    
    template<F_DECL>
    bool UEvent<F_PARAM>::isEqual( void *inObject, void *inPtr )
    {
        return (object == inObject && function == inPtr);
    }
    
    template<F_DECL>
    void UEvent<F_PARAM>::operator()( F_PARAM inParams )
    {
        switch( queueState ) {
            case QueueDisabled: {
                runEvent(inParams...);
                break;
            }
                
            case QueueStatic: {
                UQueueCallback<UEventQueue> *qs = new UQueueCallback<UEventQueue>();
                qs->set( [=](UEventQueue*){runEvent(inParams...);} );
                queue->add( qs );
                break;
            }
                
            case QueueDynamic: {
                //  We update queue every call,
                //  because we can move object via context(threads & queues)
                //  !!! thread safe by uobject realization !!!
                queue = ((UObject*)object)->getDispatchQueue();
                if( queue == NULL ) {
                    runEvent(inParams...);
                    break;
                }
                
                UQueueCallback<UEventQueue> *qs = new UQueueCallback<UEventQueue>();
                qs->set( [=](UEventQueue*){runEvent(inParams...);} );
                queue->add( qs );
                break;
            }
        }
    }
    
    template<F_DECL>
    void UEvent<F_PARAM>::operator=( UEvent<F_PARAM> &inFunction )
    {
        disconnect();
        object      = inFunction.object;
        function    = inFunction.function;
        queue       = inFunction.queue;
    }
    
    template<F_DECL>
    template<typename...T>
    void UEvent<F_PARAM>::runEvent( T... inParams )
    {
        if( object != NULL ) {
            (object->*function)(inParams...);
        }
        else {
//            AppLogQueue << AppLog.warning( "U:Event" )
//                        << "Event object = NULL"
//                        << AppLog.end();
        }
    }
}

#endif
