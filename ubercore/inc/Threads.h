#ifndef __UC_THREAD__
#define __UC_THREAD__

#include "Base.h"
#include "Events.h"
#include "Timer.h"



namespace ubercore
{
    //  ------------------------------------------------------------------------
    //  UBERCORE THREAD CLASS
    //  ------------------------------------------------------------------------    
    class UThread: public UContext
    {
    public:
        UThread ();
        UThread ( const char *inThreadName );
        ~UThread();
        
        const char*                     threadName  ();
        
        template<class T> void          bind        ( T *inObject, void (T::*inFunction)(UThread*) );
        void                            start       ();
        void                            stop        ();
        void                            wait        ();
        bool                            isWorking   ();
        
        UEventQueue*                    queue   ();
        UContext*                       context ();
        
        template<class _Block> void     dispatchBlock   ( _Block &&inBlock );
        void                            dispatch        ( UQueueEvent *inEvent );
        template<class T,typename... P>
        void                            dispatch        ( T *inObject, void (T::*inFunction)(P...), P... inParams );
        
        static sys_thread               currentThread();
        bool                            equal   ( sys_thread inT1 );
        bool                            notequal( sys_thread inT1 );
        static bool                     equal   ( sys_thread inT1, sys_thread inT2 );
    protected:
        typedef UThread super;
        UEventQueue eventQueue;
        bool        state;
    private:
        UEvent<UThread*>        proc;
        const char*             name;
        
#       ifdef USYS_WINDOWS
        HANDLE                  thread;
#       endif
        #ifdef USYS_PLATFORM_UNIX
        pthread_t               thread;
        #endif
    
        static void*    threadMain( void *inParam );
        
        //  Context Private API
        UEventQueue*    _getDispatchQueue();
        sys_thread      _getThreadId();
    };

    //  PUBLIC
    //  ------------------------------------------------------------------------

    template<class T>
    void UThread::bind( T *inObject, void (T::*inFunction)(UThread*) )
    {
        proc.bind<T>( inObject, inFunction );
    }
    
    template<class _Block>
    void UThread::dispatchBlock( _Block &&inBlock )
    {
        _Block *_blk = new _Block( inBlock );
        UQueueCallback<UThread> *sig = new UQueueCallback<UThread>;
        sig->set<_Block>( _blk, this );
        
        dispatch( sig );
    }
    
    template<class T,typename... P>
    void UThread::dispatch( T *inObject, void (T::*inFunction)(P...), P... inParams )
    {
        UQueueCallback<UThread> *sig = new UQueueCallback<UThread>;
        sig->set(
                 [=](UThread*) {
                     (inObject->*inFunction)(inParams...);
                 });        
        dispatch( sig );
    }

    

    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE AUTO THREAD CLASS
    //  ------------------------------------------------------------------------
    class UEventThread: public UThread
    {
    public:
        UEventThread();
        UEventThread( const char *inThreadName );
        
        void addTimer   ( UTimer *inTimer );
        void removeTimer( UTimer *inTimer );
        void setThreadTimeResolution( i32 inTimeResoulution );
    private:
        list<UTimer*>   timers;
        ULock           cs;
        i32             timeResolution;
        
        void hEventProc( UThread *inThread );
    };
}

#endif
