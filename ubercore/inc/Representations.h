#ifndef __UC_REPRESENTATIONS__
#define __UC_REPRESENTATIONS__

#include "Base.h"
#include "Streams.h"



namespace ubercore
{
    namespace representations
    {
        //  --------------------------------------------------------------------
        //  CREATE POINTER REPRESENTATION OBJECT
        //  --------------------------------------------------------------------
        template<typename T> void operator_create( UStream&, T **inPtr )
        {
            (*inPtr) = new T();
        }
        
        
        
        
        
        //  --------------------------------------------------------------------
        //  STRING REPRESENTATION
        //  --------------------------------------------------------------------
        UStream& operator << ( UStream &inStream, string &inString );
        UStream& operator << ( UStream &inStream, const string &inString );
        UStream& operator >> ( UStream &inStream, string &inString );
        UStream& operator << ( UStream &inStream, const char* inString );
        
        
        
        
        //  --------------------------------------------------------------------
        //  VECTOR REPRESENTATION
        //  --------------------------------------------------------------------
        template<typename T> UStream& operator << ( UStream &inStream, vector<T> &inVector )
        {
            u32 s = (u32)inVector.size();
            inStream.write( U_PKG(s) );
            for( auto &v : inVector ) {
                inStream << v;
            }
            return inStream;
        }
        
        template<typename T> UStream& operator >> ( UStream &inStream, vector<T> &inVector )
        {
            u32 s = 0;
            inStream.read( U_PKG(s) );
            for( u32 i = 0; i < s; i++ ) {
                T v;
                inStream >> v;
                inVector.push_back(v);
            }
            return inStream;
        }
        
        template<typename T> UStream& operator << ( UStream &inStream, vector<T*> &inVector )
        {
            u32 s = (u32)inVector.size();
            inStream.write( U_PKG(s) );
            for( auto &v : inVector ) {
                inStream << (*v);
            }
            return inStream;
        }
        
        template<typename T> UStream& operator >> ( UStream &inStream, vector<T*> &inVector )
        {
            u32 s = 0;
            inStream.read( U_PKG(s) );
            for( u32 i = 0; i < s; i++ ) {
                T *v; operator_create(inStream,&v);
                inStream >> (*v);
                inVector.push_back(v);
            }
            return inStream;
        }
        
        
        
        
        
        
        
        //  --------------------------------------------------------------------
        //  LIST REPRESENTATION
        //  --------------------------------------------------------------------
        template<typename T> UStream& operator << ( UStream &inStream, list<T> &inList )
        {
            u32 s = (u32)inList.size();
            inStream.write( U_PKG(s) );
            for( auto &v : inList ) {
                inStream << v;
            }
            return inStream;
        }
        
        template<typename T> UStream& operator >> ( UStream &inStream, list<T> &inList )
        {
            u32 s = 0;
            inStream.read( U_PKG(s) );
            for( u32 i = 0; i < s; i++ ) {
                T v;
                inStream >> v;
                inList.push_back( v );
            }
            return inStream;
        }
        
        template<typename T> UStream& operator << ( UStream &inStream, list<T*> &inList )
        {
            u32 s = (u32)inList.size();
            inStream.write( U_PKG(s) );
            for( auto &v : inList ) {
                inStream << (*v);
            }
            return inStream;
        }
        
        template<typename T> UStream& operator >> ( UStream &inStream, list<T*> &inList )
        {
            u32 s = 0;
            inStream.read( U_PKG(s) );
            for( u32 i = 0; i < s; i++ ) {
                T *v; operator_create(inStream,&v);
                inStream >> (*v);
                inList.push_back(v);
            }
            return inStream;
        }
        
        
        
        
        //  --------------------------------------------------------------------
        //  MAP REPRESENTATION
        //  --------------------------------------------------------------------
        template<typename K,typename V> UStream& operator << ( UStream &inStream, map<K,V> &inMap )
        {
            u32 s = (u32)inMap.size();
            inStream.write( U_PKG(s) );
            for( auto &v : inMap ) {
                inStream << v.first;
                inStream << v.second;
            }
            return inStream;
        }
        
        template<typename K,typename V> UStream& operator >> ( UStream &inStream, map<K,V> &inMap )
        {
            u32 s = 0;
            inStream.read( U_PKG(s) );
            for( u32 i = 0; i < s; i++ ) {
                K key;
                inStream >> key;
                
                V value;
                inStream >> value;
                
                inMap.insert( pair<K,V>(key,value) );
            }
            return inStream;
        }
        
        template<typename K,typename V> UStream& operator << ( UStream &inStream, map<K,V*> &inMap )
        {
            u32 s = (u32)inMap.size();
            inStream.write( U_PKG(s) );
            for( auto &v : inMap ) {
                inStream << v.first;
                inStream << *(v.second);
            }
            return inStream;
        }
        
        template<typename K,typename V> UStream& operator >> ( UStream &inStream, map<K,V*> &inMap )
        {
            u32 s = 0;
            inStream.read( U_PKG(s) );
            for( u32 i = 0; i < s; i++ ) {
                K key;
                inStream >> key;
                
                V *value; operator_create( inStream, &value );
                inStream >> (*value);
                
                inMap.insert( pair<K,V*>(key,value) );
            }
            return inStream;
        }
    }
}

#endif
