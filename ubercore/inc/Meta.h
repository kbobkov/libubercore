#ifndef __UC_META__
#define __UC_META__

#include "Include.h"
#include "Base.h"


namespace ubercore
{
    //  ------------------------------------------------------------------------
    //  UBERCORE META CLASSES
    //  ------------------------------------------------------------------------
    
    template<class Base, class D> class UIsBaseClass
    {
    private:
        template<typename T> struct dummy   {};
        struct Child : D, dummy<i32>        {};
        
        static Base*                    Check( Base*     );
        template<class T> static char   Check( dummy<T>* );
    public:
        enum { Yes  = (sizeof(Check((Child*)0)) == sizeof(Base*)) };
        enum { No   = !Yes };
    };
    
    
    template<typename T> class UIsClass
    {
    private:
        typedef char One;
        typedef struct { char a[2]; } Two;
        template<typename C> static One test(int C::*);
        template<typename C> static Two test(C);
    public:
        enum { Yes  = sizeof(UIsClass<T>::test<T>(0)) == 1 };
        enum { No   = !Yes };
    };

    
    template<typename V> class UIsRepresentable
    {
    private:
        template<typename C,bool _B> struct _Genif {};
        template<typename C> struct _Genif<C,true> {
            enum { IsCorrect = UIsBaseClass<URepresentable,V>::Yes };
        };
        template<typename C> struct _Genif<C,false> {
            enum { IsCorrect = 0 };
        };
    public:
        enum { Yes  = _Genif<V,UIsClass<V>::Yes>::IsCorrect };
        enum { No   = !Yes };
    };
}

#endif
