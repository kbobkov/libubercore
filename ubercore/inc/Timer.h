#ifndef __UC_TIMER__
#define __UC_TIMER__

#include "Base.h"
#include "Events.h"



namespace ubercore
{
    //  ------------------------------------------------------------------------
    //  UBERCORE TIMER BASE CLASS
    //  ------------------------------------------------------------------------
    namespace uprivate {
        class UTimerBase
        {
        public:
            virtual void _bindTimerThread( UEventThread *inThread ) = 0;
        };
    }

    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE TIMER CLASS
    //  ------------------------------------------------------------------------
    class UTimer: public uprivate::UTimerBase
    {
    public:
        UTimer ();
        UTimer ( u32 inInterval );
        ~UTimer();
        
        template<class T> void      bind        ( T *inObject, void (T::*inFunction)(UTimer*) );
        template<class T> void      connect     ( T *inObject, void (T::*inFunction)(UTimer*), UEventQueue *inQueue = NULL );
        
        void                        setInterval ( u32 inInterval );
        void                        start       ();
        void                        stop        ();
        void                        fire        ( u32 inMsec = 0 );
        UEventThread*               thread      ();

        template<typename T> void   setUserInfo ( T *inInfo );
        template<typename T> T*     getUserInfo ();
    private:
        UEvent<UTimer*> proc;
        u32             interval;
        u32             curvalue;        
        bool            started;
        
        void            *userinfo;
        UEventThread    *timerThread;
        
        void _bindTimerThread( UEventThread *inThread );
    };

    //  PUBLIC
    //  ------------------------------------------------------------------------

    template<class T>
    void UTimer::bind( T *inObject, void (T::*inFunction)(UTimer*) )
    {
        proc.bind<T>( inObject, inFunction );
    }
    
    template<class T>
    void UTimer::connect( T *inObject, void (T::*inFunction)(UTimer*), UEventQueue *inQueue )
    {
        proc.connect<T>( inObject, inFunction, inQueue );
    }
    
    template<typename T>
    void UTimer::setUserInfo( T *inInfo )
    {
        userinfo = (void*)inInfo;
    }
    
    template<typename T>
    T* UTimer::getUserInfo()
    {
        return (T*)userinfo;
    }
    
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE TIME INTERVAL CLASS
    //  ------------------------------------------------------------------------
    class UTimeInterval
    {
    public:
        void begin();
        void end();
        i32  getInterval();
    private:
#       ifdef USYS_WINDOWS
        i32 time_b;
        i32 time_e;
#       endif

#       ifdef USYS_PLATFORM_UNIX
        timeval time_b;
        timeval time_e;
#       endif
    };
}

#endif
