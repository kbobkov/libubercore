#ifndef __UC_BASE__
#define __UC_BASE__

#include "Include.h"
#include "System.h"
#include "Log.h"



namespace ubercore
{
    class UObject;
    class UEventQueue;    
    class UThread;
    class UEventThread;
    class UStream;

    

    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE REPRESENTABLE OBJECT CLASS
    //  ------------------------------------------------------------------------
    //  All ubercore objects are representables
    class URepresentable
    {
    public:
        virtual ~URepresentable() {}
        virtual void writeRepresentation( UStream *inStream ) {}
        virtual void readRepresentation ( UStream *inStream ) {}
    };
    
    
    
    

    //  ------------------------------------------------------------------------
    //  UBERCORE EXECUTION CONTEXT CLASS
    //  ------------------------------------------------------------------------
    class UContext
    {
    public:
        virtual sys_thread      _getThreadId     () = 0;
        virtual UEventQueue*    _getDispatchQueue() = 0;
    };

    
    
    
        
    //  ------------------------------------------------------------------------
    //  UBERCORE OBJECT CLASS
    //  ------------------------------------------------------------------------
#   define UD_CLASS_CONSTRUCTOR(_Class,_Super) _Class( UContext *inContext = NULL ): _Super(inContext)
#   define UD_CLASS_DESTRUCTOR(_Class) virtual ~_Class()
    
#   define UD_CLASS_LOG_ENABLED  protected: typedef ULog        _ULocalLog;
#   define UD_CLASS_LOG_DISABLED protected: typedef ULogDummy   _ULocalLog;
    
#   define ClassLogQueue _ULocalLog::instance().use() << " [" << u_get_current_thread() << "]  "
#   define ClassLog      _ULocalLog::instance()
    
    class UObject: public URepresentable
    {
#       ifdef USYS_DEBUG
        UD_CLASS_LOG_ENABLED
#       else
        UD_CLASS_LOG_DISABLED
#       endif
    public:
        UObject();
        UObject( UContext *inContext );
        virtual ~UObject();
        
        virtual void    setContext( UContext *inContext );
        void            setTag( u32 inTag );
        
        UContext*       getContext();
        u32             getTag();
        sys_thread      getThreadId();
        UEventQueue*    getDispatchQueue();
        
        template<class _Sig, typename... T>
        inline void     emits( _Sig &inSignal, T... inParams );
        template<class _Sig, class _Recv, typename... T>
        inline void     connect( _Sig &inSignal, _Recv *inObject, void (_Recv::*inFunction)(T...), UEventQueue *inQueue = NULL  );
        template<class _Sig, class _Recv, typename... T>
        inline void     connect( _Sig &inSignal, void (_Recv::*inFunction)(T...), UEventQueue *inQueue = NULL  );
    protected:
        typedef UObject base;
    private:
        struct _PrivateInfo {
            UEventQueue *queue;
            UContext    *context;
            u32         tag;
            ULock       protect;
            _PrivateInfo(): queue(NULL), context(NULL) {}
        } _info;
    };

    //  PUBLIC
    //  ------------------------------------------------------------------------
    
    template<class _Sig, typename... T>
    void UObject::emits( _Sig &inSignal, T... inParams )
    {
        inSignal( inParams... );
    }
    
    template<class _Sig, class _Recv, typename... T>
    void UObject::connect( _Sig &inSignal, _Recv *inObject, void (_Recv::*inFunction)(T...), UEventQueue *inQueue )
    {
        inSignal.connect( inObject, inFunction, inQueue );
    }
    
    template<class _Sig, class _Recv, typename... T>
    void UObject::connect( _Sig &inSignal, void (_Recv::*inFunction)(T...), UEventQueue *inQueue )
    {
        inSignal.connect( (_Recv*)this, inFunction, inQueue );
    }

    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE RUNTIME CLASS
    //  ------------------------------------------------------------------------
    namespace uprivate {
        class UApplicationBase: public UContext
        {
        public:
            virtual void _registrateThread   ( void *inThread ) = 0;
            virtual void _unregistrateThread ( void *inThread ) = 0;
        };
        
        extern UApplicationBase *g_runtime;
    }
}

#endif
