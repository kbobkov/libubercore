#ifndef __UC_SYSTEM__
#define __UC_SYSTEM__

#include "Include.h"



namespace ubercore
{
    //  ------------------------------------------------------------------------
    //  UBERCORE SYSTEM LOCK CLASS
    //  ------------------------------------------------------------------------
    class ULock
    {
    public:
        ULock ();
        ~ULock();
        
        void lock   ();
        void unlock ();
    private:
#       ifdef USYS_WINDOWS
        CRITICAL_SECTION    lockv;
#       endif

#       ifdef USYS_PLATFORM_UNIX
        pthread_spinlock_t  lockv;
#       endif
    };
    
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE SYSTEM LOCKER CLASS
    //  ------------------------------------------------------------------------
    class ULocker
    {
    public:
        ULocker ( ULock *inLock );
        ~ULocker();
    private:
        ULock *lock;
    };
    



    
    //  ------------------------------------------------------------------------
    //  UBERCORE SYSTEM SYNC EVENT CLASS
    //  ------------------------------------------------------------------------
    const i32 uc_ConditionWaitInfinite = -1;
    const i32 uc_ConditionWaitTimeout  =  1;

    class UCondition
    {
    public:
        UCondition ();
        ~UCondition();
        
        i32     wait ( i32 inTimeout = uc_ConditionWaitInfinite );
        void    event();
    private:
#       ifdef USYS_WINDOWS
        HANDLE          eventh;
#       endif

#       ifdef USYS_PLATFORM_UNIX
        pthread_cond_t  eventh;
        pthread_mutex_t lock;
        bool            state;
#       endif        
    };
    
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE SYSTEM FUTURE OBJECT TEMPLATE CLASS
    //  ------------------------------------------------------------------------
    template<typename T> class UFuture
    {
    public:
        UFuture();
        UFuture( const UFuture<T> &inFuture );
        
        void        set( T &inData );
        void        set( const T &inData );
        void        set( T &&inData );
        T&          get();
        T*          operator->();
        void        operator=( const UFuture<T> &inFuture );
        void        free();
        
        UFuture<T>* copy();
    private:
        struct _Data {
            T           v;
            UCondition  s;
            bool        readed;
        } *data;
        
        UFuture( _Data *inData );
    };
    
    //  PUBLIC
    //  ------------------------------------------------------------------------
    
    template<typename T>
    UFuture<T>::UFuture()
    {
        data = new _Data();
        data->readed = false;
    }
    
    template<typename T>
    UFuture<T>::UFuture( const UFuture<T> &inFuture ) :
        data( inFuture.data )
    {
    }
    
    template<typename T>
    void UFuture<T>::set( const T &inData )
    {
        data->v = T(inData);
        data->s.event();
    }
    
    template<typename T>
    void UFuture<T>::set( T &inData )
    {
        data->v = T(inData);
        data->s.event();
    }
    
    template<typename T>
    void UFuture<T>::set( T &&inData )
    {
        data->v = T(inData);
        data->s.event();
    }
    
    template<typename T>
    T& UFuture<T>::get()
    {
        if( !data->readed ) {
            data->s.wait();
            data->readed = true;
        }
        return data->v;
    }
    
    template<typename T>
    T* UFuture<T>::operator->()
    {
        if( !data->readed ) {
            data->s.wait();
            data->readed = true;
        }
        return &(data->v);
    }
    
    template<typename T>
    void UFuture<T>::operator=( const UFuture<T> &inFuture )
    {
        if( data != NULL ) {
            delete data;
        }
        data = inFuture.data;
    }
    
    template<typename T>
    void UFuture<T>::free()
    {
        if( data != NULL ) {
            delete data;
        }
    }

    template<typename T>
    UFuture<T>* UFuture<T>::copy()
    {
        return new UFuture<T>( data );
    }
    
    //  PRIVATE
    //  ------------------------------------------------------------------------
    
    template<typename T>
    UFuture<T>::UFuture( UFuture<T>::_Data *inData ) :
        data( inData )
    {
    }

    
    

    
    //  ------------------------------------------------------------------------
    //  UBERCORE SYSTEM FUNCTIONS
    //  ------------------------------------------------------------------------
#   ifdef USYS_WINDOWS
#       define  u_sleep(A) Sleep(A);
        sys_thread  u_get_current_thread();
#   endif

#   ifdef USYS_PLATFORM_UNIX
        void        u_sleep( unsigned long inMs );
        sys_thread  u_get_current_thread();
#   endif
}

#endif
