#ifndef __UC_LOG__
#define __UC_LOG__

#include "System.h"



namespace ubercore
{
    //  ------------------------------------------------------------------------
    //  UBERCORE LOG CLASS
    //  ------------------------------------------------------------------------
#   define UD_LOG_OUTPUT_FILE                   new ULogFile()
#   define UD_LOG_OUTPUT_FILE_WITH_OUTPUT(A)    new ULogFile(A)
#   define UD_LOG_OUTPUT_CONSOLE                new ULogConsole()
    
    class ULog
    {
    protected: struct State;
    public:
        ULog() {}
        virtual ~ULog() {}
        
        static ULog&                instance();
        ULog&                       use();
        
        ULog&                       operator <<( State inParam );
        template<typename T> ULog&  operator <<( T &inParam );
        template<typename T> ULog& 	operator <<( const T &inParam );

        State                       error   ( const char *inModule );
        State                       warning ( const char *inModule );
        State                       info    ( const char *inModule );
        State                       message ( const char *inModule );
        State                       end     ();
    protected:
        struct State
        {
            char *module;
            int   type;
            State( const char *inModule, int inType );
        };
        
        ostream *stream;
    private:
        ULock lock;
        virtual void writeState ( State   &inParam ) = 0;
    };
    
    //  PUBLIC
    //  ------------------------------------------------------------------------

    template<typename T>
    ULog& ULog::operator<<( const T &inParam )
    {
        (*stream) << inParam;
        return (*this);
    }
    
    template<typename T>
    ULog& ULog::operator<<( T &inParam )
    {
        (*stream) << inParam;
        return (*this);
    }


    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE NULL LOG CLASS
    //  ------------------------------------------------------------------------    
    class ULogDummy
    {
    public:
        ULogDummy() {}
        
        static ULogDummy&               instance();
        ULogDummy&                      use();
        
        template<typename T> ULogDummy& operator <<( const T &inParam );
        template<typename T> ULogDummy& operator <<( T &inParam );
        
        const int                       error   ( const char* );
        const int                       warning ( const char* );
        const int                       info    ( const char* );
        const int                       message ( const char* );
        const int                       end     ();
    };
    
    //  PUBLIC
    //  ------------------------------------------------------------------------
    
    template<typename T>
    ULogDummy& ULogDummy::operator<<( const T &inParam )
    {
        return (*this);
    }
    
    template<typename T>
    ULogDummy& ULogDummy::operator<<( T &inParam )
    {
        return (*this);
    }
    


    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE CONSOLE LOG PROVIDER CLASS
    //  ------------------------------------------------------------------------
    class ULogConsole: public ULog
    {
    public:
        ULogConsole();
    private:
        void writeState ( State   &inParam );
    };



    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE HTML FILE LOG PROVIDER CLASS
    //  ------------------------------------------------------------------------
    class ULogFile: public ULog
    {
    public:
        ULogFile ();
        ULogFile ( const char *inFilename );
        ~ULogFile();
    private:
        ofstream    out;
        bool        state;

        void open       ( const char *inFilename );
        void writeState ( State   &inParam );
    };


    
    
    
    //  ------------------------------------------------------------------------
    //  LOG CONSTANTS & DEFINES
    //  ------------------------------------------------------------------------
    namespace uprivate {
        extern ULog      *g_log;
        extern ULogDummy *g_logdummy;
    }
    
#   define AppLogQueue  (uprivate::g_log->use()) << " [" << u_get_current_thread() << "]  "
#   define AppLog       (*uprivate::g_log)
}

#endif
