#ifndef __UC_APPLICATION__
#define __UC_APPLICATION__

#include "Threads.h"



namespace ubercore
{
    //  ------------------------------------------------------------------------
    //  UBERCORE APPLICATION CLASS
    //  ------------------------------------------------------------------------
    class UApplication: public uprivate::UApplicationBase
    {
    public:
        static void             initialize( ULog *inLog );
        static UApplication*    instance    ();
        static i32              execute     ();
        template<class T>
        static i32              executeWithFinalization( T &&inFinalizationBlock );
        static void             terminate   ( i32 inError = 0 );
        
        static UEventQueue*     queue   ();
        static UEventQueue*     queue   ( const char *inQueueName );
        static UEventQueue*     queue   ( sys_thread inId );
        static UThread*         thread  ( sys_thread inId );
        
        static sys_thread       mainThread  ();
        static UContext*        context     ();
    protected:
        UApplication ();
        ~UApplication();
    private:
        i32                     errcode;
        ULock                   lock;
        map<string,UThread*>    threads;
        sys_thread              mainThreadId;
        UEventQueue             mainQueue;
        
        //  Protected API
        UEventQueue*    _getDispatchQueue();
        sys_thread      _getThreadId();
        void            _registrateThread   ( void *inThread );
        void            _unregistrateThread ( void *inThread );
    };
    
    template<class T>
    i32 UApplication::executeWithFinalization( T &&inFinalizationBlock )
    {
        instance()->mainThreadId = UThread::currentThread();
        while( instance()->errcode == -1 ) {
            instance()->mainQueue.wait();
            instance()->mainQueue.process();
        }
        inFinalizationBlock();
        return instance()->errcode;
    }

    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE APPLICATION THREADING API
    //  ------------------------------------------------------------------------    
    UEventQueue* u_dispatch_get_main_queue  ();
    UEventQueue* u_dispatch_get_queue       ( const char *inQueueName );

    template<class _Block> void u_dispatch_async( UEventQueue *inQueue, _Block &&inBlock )
    {
        if( inQueue == NULL ) {
            return;
        }
        
        _Block *blk                         = new _Block( inBlock );
        UQueueCallback<UEventQueue> *sig    = new UQueueCallback<UEventQueue>;
        sig->set( blk, inQueue );
        
        inQueue->add( sig );
    }
}

#endif
