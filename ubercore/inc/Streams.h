#ifndef __UC_STREAMS__
#define __UC_STREAMS__

#include "Meta.h"
#include "Log.h"
#include "Base.h"



namespace ubercore
{
    class UStream;
    class UFileStream;
    class UMemoryStream;
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE STREAM BASE CLASS
    //  ------------------------------------------------------------------------
#   define UD_CLASS_STATE_REPRESENTABLE private: friend class ubercore::UStream;
#   define U_PKG_PTR(V) (char*)V,  sizeof(V)
#   define U_PKG(V)     (char*)&V, sizeof(V)
    
    class UStream
    {
    public:
        //  Binary Read/Write
        virtual void                read    ( char *inBuffer, int inSize ) {}
        virtual void                write   ( char *inBuffer, int inSize ) {}
        template<typename T> void   read    ( T &inData );
        template<typename T> void   write   ( T &inData );
        template<typename T> void   write   ( const T &inData );
        
        void                        _readRepresentation ( URepresentable *inRepresentable );
        void                        _writeRepresentation( URepresentable *inRepresentable );
    };
    
    
    //  PUBLIC
    //  ------------------------------------------------------------------------

    template<typename T>
    void UStream::read( T &inData )
    {
        read( (char*)&inData, sizeof(T) );
    }

    template<typename T>
    void UStream::write( T &inData )
    {
        write( (char*)&inData, sizeof(T) );
    }
    
    template<typename T>
    void UStream::write( const T &inData )
    {
        write( (char*)&inData, sizeof(T) );
    }


    

    
    //  ------------------------------------------------------------------------
    //  UBERCORE OBJECT REPRESENTATION OPERATORS
    //  ------------------------------------------------------------------------
    //  Private template writers/readers
    namespace uprivate {
        template<typename V,bool _B> struct UWriteRepresentation{};
        template<typename V,bool _B> struct UReadRepresentation{};
        
        template<typename V> struct UWriteRepresentation<V,true> {
            static void w( UStream &s, V *v ) {
                s._writeRepresentation( v );
            }
            
            static void w( UStream &s, const V *v ) {
                s._writeRepresentation( v );
            }
        };
        template<typename V> struct UWriteRepresentation<V,false> {
            static void w( UStream &s, V *v ) {
                u32 sz = sizeof(V);
                s.write( (char*)&sz, sizeof(u32) );
                s.write( (*v) );
            }
            
            static void w( UStream &s, const V *v ) {
                u32 sz = sizeof(V);
                s.write( (char*)&sz, sizeof(u32) );
                s.write( (*v) );
            }
        };
        
        template<typename V> struct UReadRepresentation<V,true> {
            static void r( UStream &s, V *v ) {
                s._readRepresentation( v );
            }
        };
        
        template<typename V> struct UReadRepresentation<V,false> {
            static void r( UStream &s, V *v ) {
                u32 sz = 0;
                s.read( (char*)&sz, sizeof(u32) );
                if( sz != sizeof(V) ) {
                    AppLogQueue << AppLog.warning( "u:stream" )
                                << "Read type size mismatch"
                                << AppLog.end();
                }
                s.read( (*v) );
            }
        };
    }
    
    //  Represents base type as [size,bytes]
    //  Represents urepresentable classes calling read/writeRepresentation
    //  Base types:
    //  [
    //      char, short, int, int64, float, double,
    //      array & struct of these types
    //  ]
    namespace representations {
        template<typename T> UStream& operator << ( UStream &inStream, T &inData )
        {
            using namespace uprivate;
            UWriteRepresentation<T, UIsRepresentable<T>::Yes >::w( inStream, &inData );
            return inStream;
        }
        
        template<typename T> UStream& operator << ( UStream &inStream, const T &inData )
        {
            using namespace uprivate;
            UWriteRepresentation<T, UIsRepresentable<T>::Yes >::w( inStream, &inData );
            return inStream;
        }

        template<typename T> UStream& operator >> ( UStream &inStream, T &inData )
        {
            using namespace uprivate;
            UReadRepresentation<T, UIsRepresentable<T>::Yes >::r( inStream, &inData );
            return inStream;
        }
    }
    
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE FILE STREAM CLASS
    //  ------------------------------------------------------------------------
    const u32 uc_FileStreamRead         = ios::in  | ios::binary;
    const u32 uc_FileStreamWrite        = ios::out | ios::binary;
    const u32 uc_FileStreamReadWrite    = ios::in  | ios::out | ios::binary;
    
    class UFileStream: public UStream
    {
    public:
        ~UFileStream();

        //  *** Default stream functions ***
        virtual void    read    ( char *inBuffer, int inSize );
        virtual void    write   ( char *inBuffer, int inSize );
        
        //  *** File stream extend functions ***
        void            open    ( const char *inFilename, u32 inMode );
        void            close   ();
        bool            is_open ();
        void            seek    ( u32 inPosition );
    private:
        fstream fs;
    };
    
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE MEMORY STREAM CLASS
    //  ------------------------------------------------------------------------
    class UMemoryStream: public UStream
    {
    public:
        UMemoryStream();
        UMemoryStream( void *inMemory, u32 inSize );
        
        //  *** Default stream functions ***
        virtual void        read    ( char *inBuffer, int inSize );
        virtual void        write   ( char *inBuffer, int inSize );
        
        //  *** Memory stream extend functions ***
        void*               create  ( u32 inSize = 64, void *inData = NULL );
        void                open    ( void *inData, u32 inSize );
        u32                 size    ();
        void                seek    ( u32 inPosition );
        u32                 pos     ();
        void                close   ();
        void                free    ();
        void*               data    ( u32 inOffset = 0 );
        void                writeFromStream( UStream *inStream, u32 inSize );
    private:
        void    *chunk;
        u32     chunkSize;
        u32     realSize;
        u32     position;
    };
    
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE MEMORY STREAM REPRESENTATION
    //  ------------------------------------------------------------------------
    namespace representations {
        UStream& operator << ( UStream &inStream, UMemoryStream *inMemStream );        
        UStream& operator << ( UStream &inStream, UMemoryStream &inMemStream );
        UStream& operator >> ( UStream &inStream, UMemoryStream *inMemStream );
        UStream& operator >> ( UStream &inStream, UMemoryStream &inMemStream );
    }
}

#endif
