#ifndef __UBERCORE__
#define __UBERCORE__

#include "inc/Application.h"
#include "inc/Events.h"
#include "inc/Include.h"
#include "inc/Log.h"
#include "inc/Representations.h"
#include "inc/Streams.h"
#include "inc/Threads.h"
#include "inc/Timer.h"
using namespace ubercore;

#endif
