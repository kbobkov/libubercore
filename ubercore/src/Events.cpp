#include "../inc/Events.h"



namespace ubercore
{
    //  ------------------------------------------------------------------------
    //  UBERCORE EVENT QUEUE CLASS
    //  ------------------------------------------------------------------------
    
    //  PUBLIC
    //  ------------------------------------------------------------------------
    
    void UEventQueue::add( UQueueEvent *inEvent )
    {
        lock.lock();
        queue.push_back( inEvent );
        queueEvent.event();
        lock.unlock();
    }
    
    void UEventQueue::clear()
    {
        lock.lock();
        list<UQueueEvent*>::iterator i;
        for( i = queue.begin(); i != queue.end(); i++ ) {
            delete (*i);
        }
        queue.clear();
        lock.unlock();
    }
    
    i32 UEventQueue::wait( i32 inTimeout )
    {
        return queueEvent.wait( inTimeout );
    }
    
    void UEventQueue::fire()
    {
        queueEvent.event();
    }
    
    void UEventQueue::process( i32 inProcessCount )
    {
        list<UQueueEvent*>::iterator evitem;
        bool state = true;
        i32  i = 0;
        
        if( inProcessCount == uc_EventQueueProcessAll ) {
            state = (bool)queue.size();
            while( state ) {
                lock.lock();
                evitem = queue.begin();
                lock.unlock();
                
                (*evitem)->processEvent();
                delete (*evitem);
                
                lock.lock();
                queue.erase( evitem );
                state = (bool)queue.size();
                lock.unlock();
            }
        } else {
            state = (bool)queue.size();
            while( state && i < inProcessCount ) {
                i++;
                lock.lock();
                evitem = queue.begin();
                lock.unlock();
                
                (*evitem)->processEvent();
                delete (*evitem);
                
                lock.lock();
                queue.erase( evitem );
                state = (bool)queue.size();
                lock.unlock();
            }
        }
    }
    
    u32 UEventQueue::countOfEvents()
    {
        return (u32)queue.size();
    }
}
