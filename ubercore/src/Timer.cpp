#include "../inc/Timer.h"



namespace ubercore
{
    //  ------------------------------------------------------------------------
    //  UBERCORE TIMER CLASS
    //  ------------------------------------------------------------------------

    //  PUBLIC
    //  ------------------------------------------------------------------------

    UTimer::UTimer():
        started ( false ),
        userinfo( NULL )
    {
    }
    
    UTimer::UTimer( u32 inInterval ):
        interval( inInterval ),
        started ( false ),
        userinfo( NULL )
    {
    }
    
    UTimer::~UTimer()
    {
        stop();
    }
        
    void UTimer::setInterval( uint inInterval )
    {
        interval = inInterval;
    }

    void UTimer::start()
    {
        if( started ) {
            return;
        }        
        curvalue = 0;
        started  = true;
    }

    void UTimer::stop()
    {
        if( !started ) {
            return;
        }
        started = false;
    }
    
    void UTimer::fire( u32 inMsec )
    {
        if( started == false ) {
            return;
        }
        
        if( curvalue >= interval ) {
            curvalue = 0;
            proc( this );
        } else {
            curvalue += inMsec;
        }
    }
    
    UEventThread* UTimer::thread()
    {
        return timerThread;
    }
    
    void UTimer::_bindTimerThread( UEventThread *inThread )
    {
        timerThread = inThread;
    }

    
    

    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE TIME INTERVAL CLASS
    //  ------------------------------------------------------------------------

    //  PUBLIC
    //  ------------------------------------------------------------------------

    void UTimeInterval::begin()
    {
#       ifdef USYS_WINDOWS
        time_b = GetTickCount();
#       endif

#       ifdef USYS_PLATFORM_UNIX
        gettimeofday( &time_b, NULL );
#       endif
    }

    void UTimeInterval::end()
    {
#       ifdef USYS_WINDOWS
        time_e = GetTickCount();
#       endif

#       ifdef USYS_PLATFORM_UNIX
        gettimeofday( &time_e, NULL );
#       endif
    }

    i32 UTimeInterval::getInterval()
    {
#       ifdef USYS_WINDOWS
        return (time_e - time_b);
#       endif

#       ifdef USYS_PLATFORM_UNIX
        i32 v = (time_e.tv_usec - time_b.tv_usec)/1000.0f;
        return ( v > 0 ? v: v+1000 );
#       endif
    }
}
