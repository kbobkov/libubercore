#include "../inc/Base.h"



namespace ubercore
{
    namespace uprivate {
        UApplicationBase *g_runtime = NULL;
    }

    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE OBJECT CLASS
    //  ------------------------------------------------------------------------
    
    //  PUBLIC
    //  ------------------------------------------------------------------------
    
    UObject::UObject()
    {
    }
    
    UObject::UObject( UContext *inContext )
    {
        setContext( inContext );
    }
    
    UObject::~UObject()
    {
    }
    
    void UObject::setContext( UContext *inContext )
    {
        if( inContext != NULL ) {
            ULocker _Locker( &_info.protect );
            _info.context   = inContext;
            _info.queue     = inContext->_getDispatchQueue();
        }
    }
    
    void UObject::setTag( u32 inTag )
    {
        _info.tag = inTag;
    }
    
    UContext* UObject::getContext()
    {
        ULocker _Locker( &_info.protect );
        return _info.context;
    }
    
    u32 UObject::getTag()
    {
        return _info.tag;
    }
    
    UEventQueue* UObject::getDispatchQueue()
    {
        ULocker _Locker( &_info.protect );
        return _info.queue;
    }
    
    sys_thread UObject::getThreadId()
    {
        ULocker _Locker( &_info.protect );
        if( _info.context != NULL ) {
            _info.context->_getThreadId();
        }
        return u_get_current_thread();
    }
    

    
}
