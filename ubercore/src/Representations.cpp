#include "../inc/Representations.h"


namespace ubercore
{
    namespace representations
    {
        //  --------------------------------------------------------------------
        //  STRING REPRESENTATION
        //  --------------------------------------------------------------------
        UStream& operator << ( UStream &inStream, string &inString )
        {
            u32 s = (u32)inString.length();
            inStream.write( U_PKG(s) );
            inStream.write( (char*)inString.c_str(), s );
            return inStream;
        }
        
        UStream& operator << ( UStream &inStream, const string &inString )
        {
            u32 s = (u32)inString.length();
            inStream.write( U_PKG(s) );
            inStream.write( (char*)inString.c_str(), s );
            return inStream;
        }
        
        UStream& operator >> ( UStream &inStream, string &inString )
        {
            u32   s = 0;
            char *m = NULL;
            
            inStream.read( U_PKG(s) );
            m = (char*)malloc( s );
            inStream.read( m, s );
            inString.append( m, s );
            free( m );
            
            return inStream;
        }
        
        UStream& operator << ( UStream &inStream, const char *inString )
        {
            u32 s = (u32)strlen(inString);
            inStream.write( U_PKG(s) );            
            inStream.write( (char*)inString, s );
            return inStream;
        }
    }
}