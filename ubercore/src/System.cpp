#include "../inc/System.h"

namespace ubercore
{
    //  ------------------------------------------------------------------------
    //  UBERCORE SYSTEM SPINLOCK REALIZATION FOR
    //  MAC OS X 
    //  ------------------------------------------------------------------------
    
#   ifdef USYS_PLATFORM_APPLE
    static inline int pthread_spin_init( pthread_spinlock_t *lock, int pshared )
    {
        __asm__ __volatile__ ("" ::: "memory");
        *lock = 0;
        return 0;
    }
    
    static inline int pthread_spin_destroy( pthread_spinlock_t *lock )
    {
        return 0;
    }
    
    static inline int pthread_spin_lock(pthread_spinlock_t *lock)
    {
        while( true ) {
            int i;
            for( i = 0; i < 10000; i++ ) {
                if( __sync_bool_compare_and_swap(lock, 0, 1) ) {
                    return 0;
                }
            }
            sched_yield();
        }
    }
    
//    static inline int pthread_spin_trylock( pthread_spinlock_t *lock )
//    {
//        if( __sync_bool_compare_and_swap(lock, 0, 1) ) {
//            return 0;
//        }
//        return EBUSY;
//    }
    
    static inline int pthread_spin_unlock( pthread_spinlock_t *lock )
    {
        __asm__ __volatile__ ("" ::: "memory");
        *lock = 0;
        return 0;
    }    
#   endif
    

    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE SYSTEM LOCK CLASS
    //  ------------------------------------------------------------------------

    //  PUBLIC
    //  ------------------------------------------------------------------------
    
    ULock::ULock()
    {
#       ifdef USYS_WINDOWS
        InitializeCriticalSection( &lockv );
#       endif
        
#       ifdef USYS_PLATFORM_UNIX
        pthread_spin_init( &lockv, 0 );
#       endif
    }
    
    ULock::~ULock()
    {
#       ifdef USYS_WINDOWS
        DeleteCriticalSection( &lockv );
#       endif
        
#       ifdef USYS_PLATFORM_UNIX
        pthread_spin_destroy( &lockv );
#       endif
    }
    
    void ULock::lock()
    {
#       ifdef USYS_WINDOWS
        EnterCriticalSection( &lockv );
#       endif

#       ifdef USYS_PLATFORM_UNIX
        pthread_spin_lock( &lockv );
#       endif
    }

    void ULock::unlock()
    {
#       ifdef USYS_WINDOWS
        LeaveCriticalSection( &lockv );
#       endif

#       ifdef USYS_PLATFORM_UNIX
        pthread_spin_unlock( &lockv );
#       endif
    }
    
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE SYSTEM LOCKER CLASS
    //  ------------------------------------------------------------------------
    ULocker::ULocker( ULock *inLock ) :
        lock( inLock )
    {
        if( lock ) {
            lock->lock();
        }
    }
    
    ULocker::~ULocker()
    {
        if( lock ) {
            lock->unlock();
        }
    }
    

    


    //  ------------------------------------------------------------------------
    //  UBERCORE SYSTEM EVENT CLASS
    //  ------------------------------------------------------------------------

    //  PUBLIC
    //  ------------------------------------------------------------------------

    UCondition::UCondition()
    {
#       ifdef USYS_WINDOWS
        eventh = CreateEvent( NULL, true, false, NULL );
#       endif
        
#       ifdef USYS_PLATFORM_UNIX
        state = false;        
        pthread_mutex_init( &lock,   0 );
        pthread_cond_init ( &eventh, 0 );
#       endif
    }
    
    UCondition::~UCondition()
    {
#       ifdef USYS_PLATFORM_UNIX
        pthread_mutex_destroy   ( &lock   );
        pthread_cond_destroy    ( &eventh );
#       endif
    }
    
    i32 UCondition::wait( i32 inTimeout )
    {
        i32 rc;

#       ifdef USYS_WINDOWS
        WaitForSingleObject( eventh, inTimeout );
        ResetEvent( eventh );
#       endif

#       ifdef USYS_PLATFORM_UNIX
        pthread_mutex_lock( &lock );
        if( inTimeout == uc_ConditionWaitInfinite ) {
            while( !state ) {
                pthread_cond_wait( &eventh, &lock );
            }
            rc = 0;
        } else {
            timeval tvTimeout = { (i32)inTimeout / 1000, (i32)(inTimeout % 1000) * 1000 };

            timeval tvs;
            gettimeofday( &tvs, 0 );
            timeval tve;
            timeradd( &tvs, &tvTimeout, &tve );
        
            timespec tse;
            tse.tv_sec  = tve.tv_sec;
            tse.tv_nsec = tve.tv_usec * 1000;
            
            rc = 0;
            while( !state ) {
                rc = pthread_cond_timedwait( &eventh, &lock, &tse );
                if( rc == ETIMEDOUT ) {
                    rc = uc_ConditionWaitTimeout;
                    break;
                }
            }
        }

        state = false;
        pthread_mutex_unlock( &lock );
#       endif

        return rc;
    }

    void UCondition::event()
    {
#       ifdef USYS_WINDOWS
        SetEvent( eventh );
#       endif

#       ifdef USYS_PLATFORM_UNIX
        pthread_mutex_lock( &lock );
        state = true;
        pthread_cond_signal( &eventh );
        pthread_mutex_unlock( &lock );        
#       endif
    }


    


    //  ------------------------------------------------------------------------
    //  UBERCORE SYSTEM FUNCTIONS
    //  ------------------------------------------------------------------------
#   ifdef USYS_WINDOWS
    sys_thread u_get_current_thread()
    {
        return GetCurrentThread();
    }
#   endif
    
#   ifdef USYS_PLATFORM_UNIX
    void u_sleep( unsigned long inMs )
    {
        while( inMs-- ) usleep(1000);
    }
    
    sys_thread u_get_current_thread()
    {
        return pthread_self();
    }
#   endif
}
