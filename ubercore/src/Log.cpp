#include "../inc/Log.h"



//  ----------------------------------------------------------------------------
//  UBERCORE LOG CONSTANTS
//  ----------------------------------------------------------------------------
static const char* const Types[] = { "E", "W", "I", "M" };
static const char* const Colors[] =
{
   "b31111",
   "448ddf",
   "42ae1e",
   "777575"
};



namespace ubercore
{
    //  ------------------------------------------------------------------------
    //  UBERCORE LOG GLOBAL VARIABLES
    //  ------------------------------------------------------------------------
    namespace uprivate {
        ULog        *g_log;
        ULogDummy   *g_logdummy;
    }
    
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE LOG CLASS
    //  ------------------------------------------------------------------------
    
    //  PUBLIC
    //  ------------------------------------------------------------------------    
    
    ULog& ULog::instance()
    {
        return (*uprivate::g_log);
    }
    
    ULog& ULog::use()
    {
        lock.lock();
        return (*this);
    }
    
    ULog& ULog::operator <<( State inParam )
    {
        writeState( inParam );
        if( inParam.type == 4 ) {
            lock.unlock();
        }        
        return (*this);
    }

    ULog::State ULog::error( const char *inModule )
    {
        return State(inModule,0);
    }

    ULog::State ULog::warning( const char *inModule )
    {
        return State(inModule,1);
    }

    ULog::State ULog::info( const char *inModule )
    {
        return State(inModule,2);
    }

    ULog::State ULog::message( const char *inModule )
    {
        return State(inModule,3);
    }

    ULog::State ULog::end()
    {
        return State("",4);
    }

    //  PRIVATE
    //  ------------------------------------------------------------------------
    
    ULog::State::State( const char *inModule, int inType ) :
        module ( (char*)inModule ),
        type   ( inType )
    {
    }
    

    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE NULL LOG CLASS
    //  ------------------------------------------------------------------------
    
    //  PUBLIC
    //  ------------------------------------------------------------------------
    
    ULogDummy& ULogDummy::instance()
    {
        return (*uprivate::g_logdummy);
    }
    
    ULogDummy& ULogDummy::use()
    {
        return (*this);
    }  
    
    const int ULogDummy::error( const char* )
    {
        return 0;
    }
    
    const int ULogDummy::warning( const char* )
    {
        return 0;
    }
    
    const int ULogDummy::info( const char* )
    {
        return 0;
    }
    
    const int ULogDummy::message( const char* )
    {
        return 0;
    }
    
    const int ULogDummy::end()
    {
        return 0;
    }
    
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE CONSOLE LOG PROVIDER CLASS
    //  ------------------------------------------------------------------------

    //  PUBLIC
    //  ------------------------------------------------------------------------
    
    ULogConsole::ULogConsole()
    {
        stream = &cout;
        cout << setiosflags( ios::left );
    }
    
    //  PRIVATE
    //  ------------------------------------------------------------------------

    void ULogConsole::writeState( State &inParam )
    {
        if( inParam.type == 4 ) {
            cout << endl;
            return;
        }

        cout << "[" << Types[inParam.type] << "] "
             << setw(15) << inParam.module << " : ";
    }
    

    


    //  ------------------------------------------------------------------------
    //  UBERCORE HTML LOG PROVIDER CLASS
    //  ------------------------------------------------------------------------

    //  PUBLIC
    //  ------------------------------------------------------------------------
    
    ULogFile::ULogFile() :
        ULog()
    {
        open( "out.htm" );
    }
    
    ULogFile::ULogFile( const char *inFilename ):
        ULog()
    {
        open( inFilename );
    }
    
    ULogFile::~ULogFile()
    {
        if( state ) out << "</font>";
        out << "</font>";
        
        out.flush();
        out.close();
    }
    
    //  PRIVATE
    //  ------------------------------------------------------------------------

    void ULogFile::writeState( State &inParam )
    {
        if( inParam.type == 4 ) {
            out << endl;
            return;
        }

        state = true;
        out << "<font color=" << Colors[inParam.type] << ">"
            << "<font color=777575> [" << Types[inParam.type] << "]</font> "
            << setw(15) << inParam.module << "</font>"
            << "<font color=" << Colors[3] << ">" << " : ";
    }

    void ULogFile::open( const char *inFilename )
    {
        state = false;        
        out.open( inFilename, ios_base::out );
        out << setiosflags( ios::left );
        out << "<font size=\"2\" face=\"Lucida Console\"><PRE>\n";
        stream = &out;
    }
}
