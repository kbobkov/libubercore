#include "../inc/Threads.h"


#ifdef USYS_PLATFORM_UNIX
u64 __ubercoreGetThreadId( pthread_t ptid ) {
    u64 threadId = 0;
    memcpy(&threadId, &ptid, std::min(sizeof(threadId), sizeof(ptid)));
    return threadId;
}
#endif

namespace ubercore
{    
    //  ------------------------------------------------------------------------
    //  UBERCORE THREAD CLASS
    //  ------------------------------------------------------------------------

    //  PUBLIC
    //  ------------------------------------------------------------------------
        
    UThread::UThread() :
        UContext(),
        name    ( "U:Thread" ),
    	state   ( false )
    {
    }
    
    UThread::UThread( const char* inThreadName ) :
        UContext(),
        name    ( inThreadName ),
    	state   ( false )
    {
    }
    
    UThread::~UThread()
    {
        if( !state ) {
            return;
        }
        
        stop();
        wait();
    }
    
    const char* UThread::threadName()
    {
        return name;
    }

    void UThread::start()
    {
        if( proc.isEmpty() ) {
            return;
        }
        
        uprivate::g_runtime->_registrateThread( this );
        state = true;
        
#       ifdef USYS_WINDOWS
        DWORD threadId;
        thread = CreateThread( 0, 0, (LPTHREAD_START_ROUTINE)(&UThread::threadMain), this, 0, &threadId );
#       endif

#       ifdef USYS_PLATFORM_UNIX
        pthread_create( &thread, NULL, (&UThread::threadMain), this );
#       endif
    }

    void UThread::stop()
    {
        uprivate::g_runtime->_unregistrateThread( this );
        state = false;
        eventQueue.fire();
    }

    void UThread::wait()
    {
#       ifdef USYS_WINDOWS
        WaitForSingleObject( thread,  INFINITE );
#       endif

#       ifdef USYS_PLATFORM_UNIX
        pthread_join( thread, NULL );
#       endif
        
        //  clear queue &
        //  deallocate all uqueueevents
        eventQueue.clear();
    }
    
    bool UThread::isWorking()
    {
        return state;
    }
    
    UEventQueue* UThread::queue()
    {
        return &eventQueue;
    }
    
    UContext* UThread::context()
    {
        return this;
    }

    void UThread::dispatch( UQueueEvent *inEvent )
    {
        eventQueue.add( inEvent );
    }
    
    sys_thread UThread::currentThread()
    {
        return u_get_current_thread();
    }
    
    bool UThread::equal( sys_thread inT1 )
    {
#       ifdef USYS_PLATFORM_UNIX
        return (bool)pthread_equal( thread, inT1 );
#       endif
#       ifdef USYS_PLATFORM_WINDOWS
        return (thread == inT2);
#       endif
    }
    
    bool UThread::notequal( sys_thread inT1 )
    {
        return !equal( inT1 );
    }
    
    bool UThread::equal( sys_thread inT1, sys_thread inT2 )
    {
#       ifdef USYS_PLATFORM_UNIX
        return pthread_equal( inT1, inT2 );
#       endif
#       ifdef USYS_PLATFORM_WINDOWS
        return (inT1 == inT2);
#       endif
    }
    
    //  PRIVATE
    //  ------------------------------------------------------------------------
    
    void* UThread::threadMain( void *inParam )
    {
        UThread *thread = (UThread*)inParam;
        thread->proc( thread );
        return NULL;
    }
    
    UEventQueue* UThread::_getDispatchQueue()
    {
        return &eventQueue;
    }
    
    sys_thread UThread::_getThreadId()
    {
        return thread;
    }
    

    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE AUTO THREAD CLASS
    //  ------------------------------------------------------------------------

    //  PUBLIC
    //  ------------------------------------------------------------------------
    
    UEventThread::UEventThread() :
        UThread         ( "U:EventThread" ),
        timeResolution  ( 10 )
    {
        bind<UEventThread>( this, &UEventThread::hEventProc );
    }
    
    UEventThread::UEventThread( const char *inThreadName ) :
        UThread         ( inThreadName ),
        timeResolution  ( 10 )
    {
        bind<UEventThread>( this, &UEventThread::hEventProc );
    }
    
    void UEventThread::addTimer( UTimer *inTimer )
    {
        //  [Performance improvment]
        //  Thread safe than we call method
        //  from outside of timer binded thread
        
        if( this->notequal(UThread::currentThread()) ) {
            cs.lock();
            ((uprivate::UTimerBase*)inTimer)->_bindTimerThread( this );
            inTimer->start();
            timers.push_back( inTimer );
            queue()->fire();
            cs.unlock();
        }
        else {
            ((uprivate::UTimerBase*)inTimer)->_bindTimerThread( this );
            inTimer->start();
            timers.push_back( inTimer );
            queue()->fire();
        }
    }
    
    void UEventThread::removeTimer( UTimer *inTimer )
    {
        //  [Performance improvment]
        //  Thread safe than we call method
        //  from outside of timer binded thread
        
        if( this->notequal(UThread::currentThread()) ) {
            cs.lock();
            timers.remove( inTimer );
            ((uprivate::UTimerBase*)inTimer)->_bindTimerThread( NULL );
            inTimer->stop();
            cs.unlock();
        }
        else {
            timers.remove( inTimer );
            ((uprivate::UTimerBase*)inTimer)->_bindTimerThread( NULL );
            inTimer->stop();
        }
    }
    
    void UEventThread::setThreadTimeResolution( i32 inTimeResoulution )
    {
        if( this->equal(UThread::currentThread()) ) {
            timeResolution = inTimeResoulution;
        }
    }
    
    //  PRIVATE
    //  ------------------------------------------------------------------------
    
    void UEventThread::hEventProc( UThread *inThread )
    {
        list<UTimer*>::iterator it, dt;
        
        UTimeInterval   interval;
        i32             i_value = 0;
        
        while( isWorking() ) {
            interval.begin();
            cs.lock();
            if( timers.size() > 0 ) {
                for( it = timers.begin(); it != timers.end(); ) {
                    //  We can safe remove timer
                    //  because we allready ink iterator to next
                    //  item in list
                    dt = it;
                    it++;
                    (*dt)->fire( i_value );
                }
                cs.unlock();
                
                inThread->queue()->wait( timeResolution );
                inThread->queue()->process();
                
                interval.end();
                i_value = interval.getInterval();
            } else {
                cs.unlock();
                inThread->queue()->wait( uc_ConditionWaitInfinite );
                inThread->queue()->process();
                i_value = 0;
            }
        }
    }
}
