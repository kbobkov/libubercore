#include "../inc/Streams.h"


namespace ubercore
{
    //  ------------------------------------------------------------------------
    //  UBERCORE STREAM BASE CLASS
    //  ------------------------------------------------------------------------
    
    //  PUBLIC
    //  ------------------------------------------------------------------------
    
    void UStream::_readRepresentation( URepresentable *inRepresentable )
    {
        inRepresentable->readRepresentation( this );
    }
    
    void UStream::_writeRepresentation( URepresentable *inRepresentable )
    {
        inRepresentable->writeRepresentation( this );
    }
    
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE FILE STREAM CLASS
    //  ------------------------------------------------------------------------

    //  PUBLIC
    //  ------------------------------------------------------------------------
    
    UFileStream::~UFileStream()
    {
        if( is_open() ) {
            close();
        }
    }
    
    void UFileStream::open( const char *inFilename, u32 inMode )
    {
        fs.open( inFilename, inMode );
    }
    
    void UFileStream::close()
    {
        fs.close();
    }
    
    bool UFileStream::is_open()
    {
        return fs.is_open();
    }
    
    void UFileStream::seek( u32 inPosition )
    {
        fs.seekg( inPosition );
    }
    
    //  PRIVATE
    //  ------------------------------------------------------------------------
    
    void UFileStream::read( char *inBuffer, int inSize )
    {
        fs.read( inBuffer, inSize );
    }
    
    void UFileStream::write( char *inBuffer, int inSize )
    {
        fs.write( inBuffer, inSize );
    }
    
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE MEMORY STREAM CLASS
    //  ------------------------------------------------------------------------
    
    //  PUBLIC
    //  ------------------------------------------------------------------------
    
    UMemoryStream::UMemoryStream()
    {
    }
    
    UMemoryStream::UMemoryStream( void *inMemory, u32 inSize )
    {
        open( inMemory, inSize );
    }
    
    void* UMemoryStream::create( u32 inSize, void *inData )
    {
        realSize    = 0;
        chunkSize   = inSize;
        chunk       = malloc( chunkSize );
        position    = 0;
        if( inData != NULL ) {
            memcpy( chunk, inData, inSize );
        }
        return chunk;
    }
    
    void UMemoryStream::open( void *inData, u32 inSize )
    {
        realSize    = inSize;
        chunkSize   = inSize;
        chunk       = inData;
        position    = 0;
    }
    
    u32 UMemoryStream::size()
    {
        return realSize;
    }
    
    void UMemoryStream::close()
    {
        realSize    = 0;
        chunkSize   = 0;
        chunk       = NULL;
        position    = 0;
    }
    
    void UMemoryStream::free()
    {
        if( chunk != NULL ) {
            ::free( chunk );
        }
        
        realSize    = 0;
        chunkSize   = 0;
        chunk       = NULL;
        position    = 0;
    }
    
    void* UMemoryStream::data( u32 inOffset )
    {
        return ((char*)chunk + inOffset);
    }
    
    void UMemoryStream::seek( u32 inPosition )
    {
        position = inPosition;
    }
    
    u32 UMemoryStream::pos()
    {
        return position;
    }
    
    //  PRIVATE
    //  ------------------------------------------------------------------------
    
    void UMemoryStream::read( char *inBuffer, int inSize )
    {
        if( position + inSize > realSize ) {
            return;
        }
        
        memcpy( inBuffer, (char*)chunk + position, inSize );
        position += inSize;
    }
    
    void UMemoryStream::write( char *inBuffer, int inSize )
    {
        if( position + inSize > chunkSize ) {
            chunkSize = (position + inSize) * 2;
            chunk = realloc( chunk, chunkSize );
        }
        
        if( position + inSize > realSize ) {
            realSize = position + inSize;
        }
        
        memcpy( (char*)chunk + position, inBuffer, inSize );
        position += inSize;        
    }
    
    void UMemoryStream::writeFromStream( UStream *inStream, u32 inSize )
    {
        if( position + inSize > chunkSize ) {
            chunkSize = (position + inSize) * 2;
            chunk = realloc( chunk, chunkSize );
        }
        
        if( position + inSize > realSize ) {
            realSize = position + inSize;
        }
        
        inStream->read( (char*)chunk + position , inSize );
        position += inSize;
    }
    
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE MEMORY STREAM REPRESENTATION
    //  ------------------------------------------------------------------------
    namespace representations {
        UStream& operator << ( UStream &inStream, UMemoryStream *inMemStream )
        {
            if( inMemStream == NULL ) {
                return inStream;
            }
            return (inStream << (*inMemStream));
        }
        
        UStream& operator << ( UStream &inStream, UMemoryStream &inMemStream )
        {
            u32 s = inMemStream.size();
            inStream.write( U_PKG(s) );
            inStream.write( (char*)inMemStream.data(0), s );
            return inStream;
        }

        UStream& operator >> ( UStream &inStream, UMemoryStream *inMemStream )
        {
            if( inMemStream == NULL ) {
                return inStream;
            }
            return (inStream >> (*inMemStream));
        }
        
        UStream& operator >> ( UStream &inStream, UMemoryStream &inMemStream )
        {
            u32 s = 0;
            inStream.read( U_PKG(s) );
            inMemStream.writeFromStream( &inStream, s );
            return inStream;
        }
    }
}