#include "../inc/Application.h"



namespace ubercore
{
    //  ------------------------------------------------------------------------
    //  UBERCORE APPLICATION CLASS
    //  ------------------------------------------------------------------------

    //  PUBLIC
    //  ------------------------------------------------------------------------
    
    UApplication::UApplication() :
        uprivate::UApplicationBase(),
        errcode      ( -1 ),
        mainThreadId ( u_get_current_thread() )
    {
    }
    
    UApplication::~UApplication()
    {
        using namespace uprivate;
        if( g_log != NULL ) {
            delete g_log;
        }
        g_runtime = NULL;
    }
    
    void UApplication::initialize( ULog *inLog )
    {
        uprivate::g_log = inLog;
        instance();
    }

    UApplication* UApplication::instance()
    {
        using namespace uprivate;
        if( g_runtime == NULL ) {
            if( g_log == NULL ) {
                g_log = new ULogConsole();
            }
            g_logdummy    = new ULogDummy;
            g_runtime     = new UApplication();
        }        
        return (UApplication*)(g_runtime);
    }
    
    i32 UApplication::execute()
    {
        instance()->mainThreadId = UThread::currentThread();
        while( instance()->errcode == -1 ) {
            instance()->mainQueue.wait();
            instance()->mainQueue.process();
        }
        return instance()->errcode;
    }
    
    void UApplication::terminate( i32 inError )
    {
        instance()->errcode = inError;
        instance()->mainQueue.fire();
    }
    
    UEventQueue* UApplication::queue()
    {
        return &(instance()->mainQueue);
    }
    
    UEventQueue* UApplication::queue( const char *inQueueName )
    {
        ULocker _Locker( &instance()->lock );
        
        auto it = instance()->threads.find( string(inQueueName) );
        if( it != instance()->threads.end() ) {
            UEventQueue *queue = it->second->queue();
            return queue;
        }
        return NULL;
    }
    
    UEventQueue* UApplication::queue( sys_thread inId )
    {
        ULocker _Locker( &instance()->lock );
    
        for( auto &m : instance()->threads ) {
            if( m.second->equal(inId) ) {
                return m.second->queue();
            }
        }
        return NULL;
    }
    
    UThread* UApplication::thread( sys_thread inId )
    {
        ULocker _Locker( &instance()->lock );
        
        for( auto &m : instance()->threads ) {
            if( m.second->equal(inId) ) {
                return m.second;
            }
        }
        return NULL;
    }
    
    sys_thread UApplication::mainThread()
    {
        return instance()->mainThreadId;
    }
    
    UContext* UApplication::context()
    {
        return instance();
    }
    
    //  PRIVATE
    //  ------------------------------------------------------------------------
    
    UEventQueue* UApplication::_getDispatchQueue()
    {
        return &mainQueue;
    }
    
    sys_thread UApplication::_getThreadId()
    {
        return mainThreadId;
    }
    
    void UApplication::_registrateThread( void *inThread )
    {
        ULocker _Locker( &lock );
        
        UThread *th     = (UThread*)inThread;
        string thname   = string( th->threadName() );
        if( thname != "" ) {
            threads.insert( pair<string,UThread*>(thname,th) );
        }
    }

    void UApplication::_unregistrateThread( void *inThread )
    {
        ULocker _Locker( &lock );
        
        UThread *th     = (UThread*)inThread;
        string thname   = string( th->threadName() );
        if( thname != "" ) {
            threads.erase( thname );
        }
    }
 
    
    
    
    
    //  ------------------------------------------------------------------------
    //  UBERCORE APPLICATION THREADING API
    //  ------------------------------------------------------------------------
    
    UEventQueue* u_dispatch_get_main_queue()
    {
        return UApplication::queue();
    }
    
    UEventQueue* u_dispatch_get_queue( const char *inQueueName )
    {
        return UApplication::queue( inQueueName );
    }
}
